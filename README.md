# install_linux_beta

To install other software in your chromebook, follow these steps:

go to the menu at the bottom right of your screen:

![](beta_001.png)

--------------------------------------

Go to settings (the gear icon)

![](beta_002.png)

--------------------------------------

Find linux (beta) on the options to the left

![](beta_003.png)

--------------------------------------

Click on "Turn on" (if the option is not available, try restarting your chromebook)

![](beta_004.png)

--------------------------------------

Leave the username as it is, choose custom and and reduce disk size to 5gb

![](beta_005.png)

--------------------------------------

When it is done, you will see a black screen with your user name in green.

--------------------------------------

![](beta_006.png)

--------------------------------------

Then type:

**sudo apt-get update**

and hit enter (wait for it to finish)

![](beta_007.png)

--------------------------------------

Then:

**sudo apt-get install inkscape**

and hit enter

when it asks if you want to install type **y** and enter (wait for it to finish)

![](beta_008.png)
<!--- This is a markdown comment which this extension removes. 
after that, on the black screen (terminal) type this:


-->
